# Web-Oberfläche, Apps und Clients

## Die offizielle Web-Oberfläche

Diese Webanwendung, oder das Web User Interface (UI), sind sowohl mit Desktop-Browsern am PC als auch mit gängigen Browsern auf dem Smartphone nutzbar.

Weil Mastodon auf dem offenen Standard [ActivityPub](/fediverse/activitypub.html) aufbaut, gibt es eine Reihe von Möglichkeiten, mit spezialisierten Anwendungen Beiträge zu erstellen, zu veröffentlichen, zu favorisieren und zu boosten, Nutzern\_innen zu folgen etc.

Mehr dazu findet ihr unter [Einfache Benutzeroberfläche](/mastodon/ui/simple.html) bzw. [Fortgeschrittene Benutzeroberfläche](/mastodon/ui/advanced.html)

## Mobile Apps

Hier listen wir einige Anwendungen, die im Umfeld der Nutzer\_innen von rollenspiel.social bereits getestet wurden. Weitere finden Sie auf der Seite [Mobile Apps](https://joinmastodon.org/apps) der offiziellen Mastodon-Website.

## Mastdodon App Android & iOS

Die Mastodon App ([Android](https://play.google.com/store/apps/details?id=org.joinmastodon.android) & [iOS](https://apps.apple.com/us/app/mastodon-for-iphone/id1571998974)) ist sehr rudimentär gehalten und kann eine große Zahl von Funktionen die Mastodon eigentlich bietet nicht bedienen, daher ist sie nicht empfehlenswert, seht euch doch am besten eine der folgenden an:

**Die folgenden Anwendungen sind nicht Teil des Mastodon-Projekts. Daher werden neue Funktionalitäten und Verbesserungen meist zuerst in der Web-Oberfläche von Mastodon zu finden sein.**

### Android

- [**Tusky**](https://tusky.app/) - [Google Play Store](https://play.google.com/store/apps/details?id=com.keylesspalace.tusky) [kostenlos] - [F-Droid](https://f-droid.org/packages/com.keylesspalace.tusky/) [kostenlos, open-source]
- [**Fedilab**](https://fedilab.app/) - [Google Play Store](https://play.google.com/store/apps/details?id=app.fedilab.android) [kostenpflichtig] - [F-Droid](https://f-droid.org/de/packages/fr.gouv.etalab.mastodon) [kostenlos, open-source]

### iOS

- [**Metatext**](https://github.com/metabolist/metatext) - [App Store](https://apps.apple.com/app/metatext/id1523996615) [kostenlos, open-source]
- [**Toot**](https://github.com/DagAgren/toot) - [App Store](https://apps.apple.com/de/app/toot/id1229021451) [kostenpflichtig]

### Sailfish OS

- [**Tooter**](https://github.com/dysk0/harbour-tooter) - [OpenRepos](https://openrepos.net/content/dysko/tooter)

## Desktop-Clients

Grundsätzlich gilt auch hier, dass diese als Anwendungen von Drittanbietern generell nicht immer auf dem letzten Stand der Funktionalitäten des Mastodon-Projekts sind. Dafür können Sie Vorteile bieten, die die Weboberfläche prinzipiell nicht bieten kann, wie zum Beispiel die Unterstützung mehrerer Instanzen und Nutzer\_innenprofile.

- [**Whalebird**](https://whalebird.org/)
- [**Hyperspace**](https://hyperspace.marquiskurt.net/)
