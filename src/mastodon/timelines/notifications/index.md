# Mitteilungen

Wann immer jemand mit einem Ihrer Toots interagiert oder Ihnen eine Nachricht sendet, werden Sie benachrichtigt.
Die Schaltfläche "Benachrichtigungen" zeigt eine Markierung an, wenn es ungelesene Benachrichtigungen gibt, es sei denn, Sie haben dies in den Benachrichtigungseinstellungen geändert.
Klicken Sie auf die Schaltfläche "Benachrichtigungen", um alle Benachrichtigungen anstelle der Startzeitleiste anzuzeigen.

![Benachrichtigungen](img/benachrichtigungen.jpg)

## Einstellungen für Benachrichtigungen

In der oberen rechten Ecke können Sie einige Einstellungen zu den Benachrichtigungen ändern.

## Benachrichtigungen löschen

Damit werden alle Benachrichtigungen aus der Ansicht entfernt.
Sie müssen die Benachrichtigungen nicht löschen, um die Markierung für ungelesene Benachrichtigungen zu entfernen.

## Markierungen für ungelesene Benachrichtigungen

Legen Sie fest, ob die Markierung für ungelesene Benachrichtigungen auf der Schaltfläche für Benachrichtigungen angezeigt oder ausgeblendet werden soll.

## Schnellfilterleiste

Standardmäßig können Sie zwischen allen Benachrichtigungen und Erwähnungen wählen.
Wenn Sie die Schnellfilterleiste ausblenden, können Sie nicht mehr wählen und es werden nur noch alle Benachrichtigungen angezeigt.
Wenn Sie die Schnellfilterleiste einblenden und zusätzlich "Alle Kategorien anzeigen" auswählen, können Sie nur Erwähnungen, nur Likes, nur Boosts, nur Umfrageergebnisse, nur Updates von Personen, denen Sie folgen, oder nur neue Follower sehen.

## Einzelne Benachrichtigungen

Als Nächstes können Sie auswählen, welche Art von Benachrichtigung für jede Kategorie angezeigt werden soll.

### Desktop-Benachrichtigungen

Desktop-Benachrichtigungen sind Popup-Benachrichtigungen auf Ihrem Computer, die nicht nur angezeigt werden, wenn Sie sich im Browser befinden.

### Push-Benachrichtigungen

Push-Benachrichtigungen sind Benachrichtigungen auf Ihrem Handy.

### In Spalte anzeigen

Hier können Sie Benachrichtigungstypen aus Ihrer Benachrichtigungsansicht ausblenden. Wenn Sie z.B. zu viele Benachrichtigungen erhalten, können Sie "mag" ausblenden, um nur "wichtigere" Benachrichtigungen zu sehen.

### Ton abspielen

Hier können Sie einfach festlegen, ob Sie für jede Art von Benachrichtigung einen Ton erhalten.
