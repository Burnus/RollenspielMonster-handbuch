# Konto & Sicherheit

## Kontostatus

Hier steht der Status des kontos, dieser kann von uns eingeschränkt werden wenn wir sehen das du gegen die Regeln verstößt.

## Sicherheit

Hier könnten E-Mail und Kennwort geändert werden.

## Sitzungen

Zeigt an auf welchen Geräten du dich im Browser angemeldet hast, wenn du sehen wilst welche Apps angemeldet sind musst du unter [Authorisierte Anwendungen](/settings/authorisierte-anwendungen.html) nachsehen.

## Ziehe zu einem anderen Konto um

### Umzug

Du kannst innerhalb von Mastodon problemlos die Instanzen wechseln, dazu benötigst du nur ein neues Konto auf einer anderen Instanz. Folgende Punkte sind zu beachten:

- Diese Aktion wird alle Folgende vom aktuellen Konto auf das neue Konto verschieben
- Das Profil deines aktuellen Kontos wird mit einer Weiterleitungsnachricht versehen und von Suchanfragen ausgeschlossen
- Keine anderen Daten werden automatisch verschoben
- Das neue Konto muss zuerst so konfiguriert werden, dass es auf das alte Konto referenziert
- Nach dem Migrieren wird es eine Abklingzeit geben, in der du das Konto nicht noch einmal migrieren kannst
- Dein aktuelles Konto wird nachher nicht vollständig nutzbar sein. Du hast jedoch Zugriff auf den Datenexport sowie die Reaktivierung.

[GNU/Linux.ch](https://gnulinux.ch) hat einen Beitrag veröffentlich wie man bei einem Umzug vorgeht: https://gnulinux.ch/mastodon-konto-umziehen

### Weiterleitung

Alternativ kannst du auch einfach nur eine Weiterleitung einrichten, dabei wird nur eine Verlinkung erstellt. Außerdem ist folgendes zu beachten:

- Das Profil deines aktuellen Kontos wird mit einer Weiterleitungsnachricht versehen und von Suchanfragen ausgeschlossen
- Keine anderen Daten werden automatisch verschoben
- Dein aktuelles Konto wird nachher nicht vollständig nutzbar sein. Du hast jedoch Zugriff auf den Datenexport sowie die Reaktivierung.

### Schritt für Schritt Anleitung

1. Erstelle den Account zu dem du umziehen willst, wenn er nicht bereits existiert. Es muss kein neuer Account sein. Du kannst auch auf einen bereits aktiven Account umziehen und so quasi zwei Accounts vereinen.
2. Melde dich auf dem Account an ZU dem du umziehen willst und geh in die Einstellungen > Account > Von anderem Account umziehen.
3. Gib dort den Accountnamen (@profilname@instanz.domain) des Accounts VON dem du umziehen willst ein.
4. Jetzt melde dich auf dem Account an, VON dem du umziehen willst und geh in die Einstellungen > Account > Zu einer anderen Instanz umziehen.
5. Gib den Accountnamen des Accounts ZU dem du umziehen willst sowie dein Passwort ein und klicke dann auf "Follower umziehen".
6. Warte eine Weile bis alle Follower übertragen werden.

## Ziehe von einem anderen Konto um

Hier kann der oben beschriebene Kontoalias aus Schritt 3 erstellt werden.

## Konto löschen

Damit kannst du dein Konto entgültig löschen, dabei passieren folgende dinge:

- Du kannst dein Konto nicht reaktivieren
- Dein Benutzername bleibt nicht verfügbar
- Deine Beiträge und andere Daten werden dauerhaft entfernt
- Inhalte, die von anderen Servern zwischengespeichert wurden, können weiterhin bestehen

> Das Löschen von Konten ist eine dauerhafte Aktion, die **nicht** rückgängig gemacht werden kann. Der Username ist dann für immer verloren.

Ebenfalls wird die E-Mail mit der sie sich registriert haben auf eine Sperrliste gesetzt, diese können wir aber wieder freigeben wenn sie sich mit dieser an uns wenden.
