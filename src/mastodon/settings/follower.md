# Folgende und Gefolgte

Hier sehen Sie alle Personen, die Ihnen folgen, alle Personen, denen Sie folgen, oder alle Personen, denen Sie folgen.
Sie können nach Kontoaktivität filtern und nach den neuesten oder zuletzt aktiven Personen sortieren und Personen per Batch entfolgen.
