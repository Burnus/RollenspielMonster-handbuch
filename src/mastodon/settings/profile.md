# Profil

## Aussehen

### Anzeigename

Hier können Sie den Namen wählen, der auf Ihrem Profil und in Ihren Beiträgen angezeigt wird.
Ein Anzeigename kann Leerzeichen und Emoji enthalten.

### Über mich

Hier können Sie einen kurzen Text über sich schreiben, um den Besuchern Ihres Profils mitzuteilen, wer Sie sind.

### Titelbild

Das Kopfzeilenbild wird nur angezeigt, wenn Besucher Ihr Profil besuchen. Es wird oben wie ein Banner angezeigt.

### Profilbild

Der Avatar ist Ihr Profilbild, das nicht nur auf Ihrem Profil, sondern auch auf allen Ihren Toots angezeigt wird.
Ein Avatar kann transparent sein.

### Profil sperren

besser wäre: Folgeanfragen verlangen

Wählen Sie, ob Sie möchten, dass jeder Ihnen sofort folgen kann, oder ob Sie Folgeanfragen genehmigen möchten.

### Dieses Profil ist ein Bot

Diese Option bewirkt nicht viel, außer Ihr Profil als Bot zu kennzeichnen. Diese Option wird z. B. gewählt, wenn Sie überwiegend Crossposts von Twitter aus machen oder Ihre Beiträge über RSS füttern.

### Dieses Profil im Profilverzeichnis zeigen

Mit dieser Option wird Ihr Profil im Profilverzeichnis und in den Follow-Vorschlägen angezeigt.

### Netzwerk ausblenden

Blendet aus wem du folgst und wer dir folgt

### Tabellenfelder

Hier können Sie 4 Benutzerfelder definieren. Viele Leute nutzen dies, um auf ihre Homepage(s) zu verlinken oder um ihre Pronomen anzuzeigen.

### Verifizierung

Sie können einen Link in Ihren Profilfeldern verifizieren, indem Sie das Attribut rel="me" zu einem Link zurück von dieser Homepage zu Ihrem Profil hinzufügen.
Es gibt einen Link, den Sie einfach kopieren und auf Ihrer Homepage einfügen können.

### Konto umziehen, löschen

Dazu gibt es informationen an anderer [Stelle](/mastodon/settings/konto-sicherheit.html#ziehe-zu-einem-anderen-konto-um).

## Ausgewählte Hashtags

Hier können Sie bis zu 10 [Hashtag](/mastodon/toots/hashtags.html)'s auswählen, die auf Ihrem Profil angezeigt werden sollen.
