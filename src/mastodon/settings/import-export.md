# Importieren und Exportieren

Sie können die Personen, denen Sie folgen, Ihre Listen, Stummschaltungen und Sperren sowie Ihre Lesezeichen exportieren.
Diese können dann in ein anderes Profil importiert werden. Sie können auch ein Archiv Ihrer Beiträge und Medien anfordern, aber dafür gibt es keine Importfunktion, sodass Sie Ihre Beiträge nicht mitnehmen können, wenn Sie die Instanzen wechseln.
