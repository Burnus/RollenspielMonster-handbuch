# Hashtags

Hashtags sind Begriffe, die mit einem vorangestellten _Hash_ oder auch _Raute_ Zeichen (`#`) geschrieben werden. Diese werden von Mastodon als Hyperlinks dargestellt. Ein Klick auf einen solchen Hyperlink ermöglicht das Auffinden weiterer Toots, die diesen Hashtag enthalten.

**Wichtig:** Im gegensatz zu Twitter sorgt hier kein Algorithmus dafür, dass Eure Posts mehr gesehen werden. Euer Post rauscht durch die Zeitleisten. Um dafür zu sorgen das eure Beiträge genau von den Leuten gesehe werden die ihr erreichen wollt ist es Sinnvoll Hashtags zu setzen die zum Thema passen.

Hashtags können ausschließlich aus den folgenden Zeichen bestehen:

- Buchstaben A bis Z bzw. a bis z
- Ziffern von 0 bis 9
- Umlaute und vergleichbare Unicode-Zeichen
- der Unterstrich `_`

Die Groß- und Kleinschreibung ist bei Hashtags nicht relevant. Wer Groß- und Kleinschreibung mischt ([CamelCase](https://de.wikipedia.org/wiki/Binnenmajuskel#Programmiersprachen)), also beispielsweise `#RollenspielMonster` schreibt, macht damit den Hashtag allerdings leichter lesbar.
Außerdem ist es für Screenreader leichter zu lesen wen UpperCamelCase verwenet wird, d.h. der Anfangsbuchstabe eines jeden Wortes wird großgeschrieben.

## Suche nach Hashtags

Die Suche nach Beiträgen, die durch Klick auf ein verlinktes Hashtag ausgelöst wird, erfasst alle Beiträge, die rollenspiel.social bekannt sind. Das sind in der Regel die Beiträge von Profilen, denen die Nutzer\*innen von rollenspiel.social folgen.

Das Suchergebnis einer solchen Hashtag-Suche verhält sich im Mastodon Web-Interface wie eine Zeitleiste. Neue, öffentliche Toots erscheinen darin automatisch, sobald sie veröffentlicht werden.

Wenn du die [Fortgeschrittene Benutzeroberfläche](/mastodon/ui/advanced.html)benutzt kannst du die Hashtags als eigene Spalte anzeigen.
