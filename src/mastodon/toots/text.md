# Beitragstext

**Mastodon-Beiträge bzw. Toots können Text, Emoji, Bilder, Videos und Audo enhalten.**

## Maximale Länge

Die Länge eines Beitrags ist auf **500 Zeichen** beschränkt. Bei der Eingabe in der Web-Oberfläche wird Dir angezeigt, wie viele Zeichen Du noch eingeben kannst, bis Du diese Grenze erreicht hast.

Ausnahmen:

- eine URL ist immer 23 zeichen Lang
- bei [Adressierungen](/mastodon/toots/mentions.html) wird nur der Benutzername nicht der Instanznahme gezählt

Wer selten einmal längere Texte schreibt kann sogenannte [Threads](/mastodon/toots/replies-threads.html) nutzen.
Wenn ihr öfters lange Texte schreibt könnte euch vielleicht [Writefreely](/writefreely/) gefallen.

## Emoji

[Emoji](https://de.wikipedia.org/wiki/Emoji) sind Symbole und Piktogramme, die in Text eingefügt werden können. Mastodon unterstützt Emoji ähnlich, wie man es schon von vielen Social-Media-Plattformen und Messengern kennt.

Zum Einfügen von Emoji in eine neue Nachricht gibt es ein Menü. Du kannst es über einen Klick auf das ausgegraute Smiley-Icon 😂 rechts oben im Eingabefeld für den Nachrichtentext öffnen.

![Toot mit eigenen Emoji](img/emoji-menu-icon.png)

Das Menü listet Dir alle verfügbaren Emoji auf. Durch Klick auf das jeweilige Emoji fügst Du es in den Nachrichtentext ein.

![Emoji-Menü](img/emoji-menu.png)

Das Menü ist nach **Kategorien** geordnet. Über die Icons am oberen Rand des Menüs kannst Du direkt zur jeweiligen Funktion wie zum Beispiel _Personen_, _Natur_ oder _Essen und Trinken_ springen.

Darüber hinaus kannst Du über eine englischsprachige **Suche** direkt nach Emoji suchen. Hier genügt auch die teilweise Eingabe eines Suchbegriffs. Gib zum Beispiel `sunf` ein, um das Sonnenblumen-Emoji (_sunflower_) zu finden.

### Emoji-Eingabe für Profis

Mastodon unterstützt die **Eingabe von Emoji direkt über die Tastatur**, ohne Benutzung des Menüs. Dazu musst Du zumindest ungefähr den Namen des Emoji, das Du einfügen willst, kennen. Um beispielsweise die Sonnenblume in Deine Nachricht einzufügen, schreibst Du `:sunflower:`. Mastodon erleichtert Dir die Eingabe durch Anzeige eines Menüs zur Vervollständigung.

![Emoji-Eingabe mit Autovervollständigung](img/emoji-autocomplete.png)

Dieses Menü findet, genau wie die Suche im Emoji-Menü, auch Teilbegriffe mitten im Emoji-Namen. Du kannst die Sonnenblume also beispielsweise auch finden, indem Du `:flow` eingibst und dann den richtigen Treffer auswählst.

Das Menü kannst Du selbstverständlich komplett über die Tastatur bedienen, so dass Du Texte mit Emoji bequem eingeben kannst, ohne einmal die Hände von der Tastatur nehmen zu müssen.

## Eigene Emoji

Mastodon erlaubt es den Betreibern einer eigenen Instanz, die Emoji-Palette um eigene Bilder, Symbole und Piktogramme zu erweitern.

![Toot mit eigenen Emoji](img/emoji-custom.png)

Wie Du im Screenshot weiter oben sehen kannst, gibt es im Menü dafür die Kategorie _Dices_ und _RollenspielMonster_.
