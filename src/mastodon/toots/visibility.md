# Sichtbarkeit und Zielgruppen

Sofern Du es in Deinen [Profileinstellungen](https://rollenspiel.social/settings/preferences/other) nicht anderweitig eingestellt hast, ist jeder Toot, den Du absendest, automatisch für jede\*n sichtbar.

Beim Erstellen eines Beitrags bzw. Toots hast Du jedoch die Möglichkeit, die Sichtbarkeit für jeden Toot individuell zu beeinflussen.

![Menü zur Einstellung der Sichtbarkeit eines Toot](img/toot-visibility-menu.png)

⚠️ **Eine nachträgliche Änderung der Sichtbarkeit nach dem Absenden ist nicht möglich.** Hier hilft nur der Umweg über die Löschfunktion.

Die Optionen des Menüs werden in den nachfolgenden Abschnitten beschrieben.

## Öffentlich

Deine Follower können den Toot sehen. Dazu wird er anderen Nutzer\*innen, die nicht Deine Follower sein müssen, in öffentlichen Zeitleisten angezeigt. Das bedeutet, er erscheint beispielsweise in der [Lokalen Zeitleiste](https://rollenspiel.social/web/timelines/public/local) (alle Beiträge auf rollenspiel.social in chronologischer Folge) sowie in den föderierten Zeitleisten von Instanzen, die mit rollenspiel.social Beiträge austauschen.

## Nicht gelistet

Der Toot wird nicht in der lokalen oder globalen Zeitleiste angezeigt, gut für [Threads](/mastodon/toots/replies-threads.html?highlight=thread#threads). Alle, die dir folgen, bekommen den Toot in der [Startseite](/mastodon/timelines/#startseite) angezeigt und jede Nutzer\*in den Toot potenziell auf anderen Wegen finden z.B. über dein Profil.

## Nur Follower

Einen solchen Toot können nur Nutzer\*innen sehen, die Dir folgen. Sollte Nutzer\*in Dir folgen, nachdem der Beitrag abgesendet wurde, wird diese\*r den Betrag ab dann auch sehen können. Diese Toots sind auch nicht [teilbar](/mastodon/interaction/posts.html#teilen).

Diese Einstellung könnte jedoch nützlich sein, wenn Du selbst bestimmst, wer Dir folgen darf.

## Nur erwähnte Personen

Frühers Direktnachrichten; nur die Nutzer\*innen, die Du in der Nachricht erwähnst, können die Nachricht sehen.

️️⚠️ **Warnung zu Direktnachrichten**

Da Nachrichten von Mastodon ja nicht nur an Mastodon-Accounts, sondern auch an Accounts auf anderen Plattformen gesendet werden können, ergibt sich ein mögliches Problem.
Außerdem sind die nachrichten *aktuell* nicht Verschlüsselt, es ist nicht ratsam private und brisantze Daten darüber zu versenden.

Wie [fediverse.party](https://fediverse.party/en/mastodon/) schreibt:

> Nur erwähnte Personen ("private Nachrichten") werden mit eingeschränkter Sichtbarkeit (nicht öffentlich) nur zwischen Mastodon-Nutzern übermittelt. Freunde aus anderen Fediverse-Netzwerken können solche "Direktnachrichten" als öffentlich (für alle sichtbar) erhalten, da einige Netzwerke (GNU Social, postActiv) DMs nicht unterstützen. Behalten Sie das im Hinterkopf, wenn Sie private Informationen über eine DM teilen.

Falls Du vertrauliche Informationen über "Nur erwähnte Personen" versenden möchtest, solltest Du Dir also sicher sein, dass die Nutzer\*innen, die Du anschreibst, ebenfalls Mastodon nutzen.
