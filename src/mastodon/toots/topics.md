# Themen für Hashtags

Um besser das gewünschte Thema zu finden ist es sinnvoll die gleichen Hashtags zu benutzen, anbei ein paar vorschläge.

## Pen and Paper

Für den Pen and Paper bereich haben sich schon folgende eingebürgert:

- `#pnp` für allgemeine Themen rund um Pen and Paper
- `#pnpDe` für allgemeine *deutsche* Themen rund um Pen and Paper
- `#pnpSuche` für das suchen nach Gruppen oder Spielern

## Larp

- `#larp` für allgemeine Themen rund um Live Action Role Playing
- `#larpDe` für allgemeine *deutsche* Themen rund um Live Action Role Playing

## Tabletop

- `#ttRpg` für allgemeine Themen rund um Tabletop RPG

## TCG

- `#tcg` für allgemeine Themen rund um Trading Card Game`s

## Konkrete Systeme

Für Beiträge, die sich auf ein oder wenige konkrete Spielsysteme beziehen, empfielt es sich, diese mit Hashtags zu benennen. Eingebürgerte Beispiele sind:

- `#Aborea` für Aborea ([Kurzregeln herunterladen](https://www.aborea.de/download/aborea-kurzregeln-starter/#))
- `#ButterflyAspect` für Butterfly Aspect ([Regeln online lesen](https://book.butterfly-aspect.de/))
- `#dnd` für Dungeons & Dragons (unabhängig von der Edition)
- `#dnd5e` für Dungeons & Dragons fünfte Edition ([Spielerhandbuch kaufen](https://dnd.wizards.com/de/products/rpg_playershandbook)
- `#dsa` für Das Schwarze Auge ([Regeln 5. Edition online lesen](https://ulisses-regelwiki.de/start.html))
- `#pathfinder` für Pathfinder (unabhängig von der Edition)
- `#pathfinder2e` für Pathfinder zweite Edition ([Regeln online lesen, englisch](https://2e.aonprd.com/PlayersGuide.aspx))
- `#W40k` für Warhammer 40k ([Grundregeln herunterladen](https://warhammer40000.com/wp-content/uploads/2020/07/GER_40K9_Basic_Rules.pdf))
- `#WHF` für Warhammer Fantasy ([Regelwerk kaufen, englisch](https://cubicle7games.com/warhammer-fantasy-roleplay-rulebook-pdf))
