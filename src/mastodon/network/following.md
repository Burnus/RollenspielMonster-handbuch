# Folgen und Folgende

## Was bedeutet Folgen?

Einem Profil auf Mastodon zu folgen, bedeutet im Wesentlichen, die Beiträge zu abonnieren, die von diesem Profil veröffentlicht werden.

Wem Sie auf Mastodon folgen, bestimmt ganz wesentlich, welche Inhalte Sie auf der Plattform selbst zu sehen bekommen. Zwar bietet Mastodon auch zwei Zeitleisten, die nicht dadurch bestimmt werden, wem Sie folgen. Allerdings dürfte die Zeitleiste, die Ihnen als [Startseite](https://rollenspiel.social/web/timelines/home) angezeigt wird, die wichtigste sein. Denn deren Inhalte werden durch die Profile bestimmt, denen Sie folgen.

Wie schon im Abschnitt über das [ActivityPub-Protokoll](/fediverse/activitypub.html) erklärt, kannst Du nicht nur Profilen von einer beliebigen Mastodon-Instanz folgen. Du kannst auch Profilen von anderen Plattformen folgen, sofern diese ActivityPub unterstützen. Twitter unterstützt dieses Protokoll übrigens nicht, und so ist es auch leider nicht möglich, einem Twitter-Account von Mastodon aus zu folgen.

## Ein Netzwerk aufbauen

Folgen Sie einem Profil, so wird der/die gefolgte Nutzer\_in darüber benachrichtigt. Nicht selten dient das dem/der Inhaber\_in des gefolgten Profils als Anlass, sich Ihr Profil und Ihre letzten Beiträge etwas näher anzusehen und Ihnen ggf. ebenfalls zu folgen. So entsteht, zumindest im technischen Sinne, eine gegenseitige Beziehung. Diese hat zur Folge, dass der/die Betreffende selbstverständlich auch Ihre Beiträge zukünftig in der Zeitleiste vorfindet.

Möchten Sie über Mastodon Menschen erreichen, ist es also ratsam, sich selbst für andere Menschen zu interessieren und diesen zu folgen. Es sei denn, Sie sind so bekannt bzw. berühmt, dass sich ohnehin viele für Sie interessieren.

## Wie folgt man?

Praktisch geht das Folgen in Mastodon an verschiedenen Stellen:

- In den Zeitleisten: Klicke auf den Namen oder das Bild des Absender-Profils eines Beitrags, um zur Profilseite des Absenders zu gelangen. Nun gibt es mehrere Möglichkeiten:
  - Handelt es sich um ein Mastodon-Profil auf rollenspiel.social, dann gibt es dort einen Folgen-Button. Diesen klicken Sie einfach an. Fertig.
  - Handelt es sich um ein Mastodon-Profil außerhalb von rollenspiel.social, finden Sie ebenfalls einen Button "Folgen" oder "Follow", je nach Spracheinstellung der Mastodon-Instanz. Klicken Sie auf diesen, werden Sie aufgefordert, anzugeben, von welchem Profil Sie folgen möchten.
- Ist das Profil nicht in der Zeitleiste enthalten, kopieren Sie am besten den Namen samt der Instanzadresse z.B. `@name@instanz.adresse`. Diese geben Sie dann in das Suchfeld auf rollenspiel.social ein. Als Suchergebnis sollten Sie genau einen Personen-Treffer finden.
- Alternativ gibt es auch die Möglichkeit die URL zu dem Profil in die Suche einzutragen und so den gewünschten Account zu finden.

Der nachfolgende Screenshot zeigt ein Suchergebnis mit 4 Treffern. Das graue Icon auf der rechten Seite kann angeklickt werden, um dem Profil zu folgen und das blaue um das Folgen zu beenden.

![Suche](img/suche.jpg)

### Alte Beiträge

Wenn man einem Account folgt der noch keine Verbindung zur Instanz hatte werden keine alten Beiträge angezeigt.

## Profile zum Folgen finden

Profile zu finden, denen man folgen könnte, ist aufgrund der verteilten Arbeitsweise von Mastodon nicht ganz so einfach, wie man sich das vielleicht wünscht. Eine zentrale Suche, wie wir sie von Twitter kennen, gibt es bei Mastodon nicht.

Hier sind ein paar Möglichkeiten, wie Sie aktiv nach interessante Profile suchen können, denen Sie folgen können:

- Sowohl die [lokale](https://rollenspiel.social/web/timelines/public/local) als auch die [föderierte](https://rollenspiel.social/web/timelines/public) Zeitleiste enthalten in der Regel auch Beiträge von Profilen, denen Sie nicht folgen.

- Das öffentliche [Profilverzeichnis]|(https://rollenspiel.social/explore) von rollenspiel.social zeigt Profile, die mindestens 10 Folgende haben und deren Inhaber\_innen dem Erscheinen im Profilverzeichnis nicht widersprochen haben. Das funktioniert auch auf anderen Mastodon-Instanzen, wie z.B. [chaos.social](https://chaos.social/explore) oder [social.tchncs.de](https://social.tchncs.de/explore)

- Die Suche nach einem Thema oder [Hashtag](/mastodon/toots/hashtags.html), entweder auf rollenspiel.social oder auf einer anderen Instanz.

## Den Überblick behalten

Mastodon bietet Ihnen die Seite [Folgende und Gefolgte](https://rollenspiel.social/relationships) an, damit Sie schnell Profile finden können, denen Sie folgen und die Ihnen folgen. Die Seite bietet einige nützliche Funktionen, auf die wir hier näher eingehen wollen.

Unter der Beschriftung **Beziehung** bieten sich auf der Seite drei Filter-Einstellungen:

- [FOLGT](https://rollenspiel.social/relationships): Profile, denen Sie folgen.
- [FOLGENDE](https://rollenspiel.social/relationships?relationship=followed_by): Profile, die Ihnen folgen.
- [BEKANNT](https://rollenspiel.social/relationships?relationship=mutual): Profile, denen Sie folgen _und_ die Ihnen ebenfalls folgen. Also die Schnittmenge aus den oberen beiden.

Die Listen lassen sich außerdem nach **Kontostatus** einschränken.

- PRIMÄR: Profile, die nicht umgezogen sind.
- UMGEZOGEN: Profile, die angeben, zu einer anderen Instanz umgezogen zu sein.

Dadurch können Sie herausfinden, ob Sie Profilen folgen, die inzwischen nicht mehr aktiv sind, weil sie zu einer anderen Instanz umgezogen sind. In diesem Fall können Sie die Profilseite besuchen und herausfinden, wohin das Profil umgezogen ist, um dem neuen Profil zu folgen.
