# Netzwerk

## Dezentral? Verpass ich was?

Also nein, nur weil es Dezentral ist und nicht alle auf einer Instanz sind verpasst man nichts. Es besteht jederzeit die Möglichkeit anderen Accounts von anderen Instanzen zu folgen und bekommt die Nachrichten dann genauso wie wenn man die gleiche Instanz teilen würde.
Es gibt dabei aber ein paar Ausnahmen:

### Blockliste

Jede Instanz hat eine Blockliste, oft ist diese unter `/about/more#unavailable-content` zu sehen, bei rollenspiel.social wäre das [hier](https://rollenspiel.social/about/more#unavailable-content). Dort gibt es verschiedene Stufen der Blockierung: _Stummgeschaltete Server_ und _Gesperrte Server_.

#### Stummgeschaltete Server

Beiträge von diesem Server werden nirgends angezeigt, außer in Ihrer Startseite, wenn Sie der Person folgen, die den Beitrag verfasst hat.

#### Gesperrte Server

Sie können niemandem von diesem Server folgen, und keine Daten werden verarbeitet oder gespeichert und keine Daten ausgetauscht.

### Hashtags

Auch Hashtags sind nicht über das ganze Fediverse zu finden und unterliegen einer Ausnahme, mehr dazu findest du [hier](/mastodon/toots/hashtags.html#suche-nach-hashtags).
