# Interaktion mit Beiträgen

## Antwort

Das erste Symbol am unteren Rand ist die Schaltfläche "Antworten". Klicken Sie darauf, um auf den Beitrag zu antworten.
Die Person, die den Beitrag geschrieben hat, wird automatisch in Ihrer Antwort erwähnt. Entfernen Sie diese Erwähnung nicht, sonst wird die Person nicht über Ihre Antwort benachrichtigt.

### Allen Antworten

## Teilen

Das nächste Symbol ist der Boost-Button. Boosting ist das, was Retweeten, Rebloggen oder Teilen auf anderen Plattformen ist.
Wenn Sie also auf die Schaltfläche klicken, erscheint der Beitrag sowohl in Ihrem Profil als auch in den privaten Timelines Ihrer Follower.
Der/Die ursprüngliche Autor\_in des Beitrags wird benachrichtigt, dass Sie ihn geboostet haben.

## Favorisieren

Wenn Sie einen Beitrag liken, wird auch der Autor des Beitrags benachrichtigt und - wie beim Boosten - wird angezeigt, wie oft der Beitrag geliked wurde.
Ansonsten hat es eine ähnliche Funktion wie das Setzen eines Lesezeichens für einen Beitrag - Sie können alle Beiträge, die Ihnen gefallen haben, in einer separaten Ansicht sehen.

## Mehr

Die folgenden Funktionen finden Sie im Dropdown-Menü - das letzte Symbol in der Reihe.

### Diesen Beitrag öffnen

Wenn Sie einen Beitrag erweitern, wird er anstelle der Startzeitleiste angezeigt (anders in der erweiterten Oberfläche) und die Antworten werden darunter angezeigt.
Sie können auch zur erweiterten Ansicht gelangen, indem Sie einfach auf den Beitrag klicken - achten Sie aber darauf, dass Sie nicht auf eine Erwähnung, einen [Hashtag](/mastodon/toots/hashtags.html) oder einen Link klicken.

### Kopiere Link zum Beitrag

Diese Funktion kopiert einfach die direkte URL des Beitrages. Sie können diese Funktion verwenden, um den Beitrag an anderer Stelle zu teilen - z. B. in einem Messenger.

### Einbetten

Damit wird euch ein [iFrame](https://de.wikipedia.org/wiki/Inlineframe) Code erzeugt den Ihr auf Webseiten platzieren könnt um den Beitrag anzuzeigen.

---

### Lesezeichen

Das Setzen eines Lesezeichens ist wie ein "Gefällt mir", nur dass der/die Autor\_in nicht benachrichtigt wird und nicht gezählt wird, wie oft ein Beitrag mit einem Lesezeichen versehen wurde.
Sie können Ihre mit Lesezeichen versehenen Beiträge in einer anderen Ansicht sehen als Ihre gelikten Beiträge.

### Im Profil anheften

Diese Option lässt den Beitrag in deinem Profil ganz oben erscheinen und hält ihn dort, es können nur 5 Beiträge angeheftet werden.

---

### Konversation stummschalten

placeholder

---

### @KONTONAME erwähnen

Mit dieser Funktion wird ein neuer Beitrag erstellt, in dem der/die Autor\_in des Toots erwähnt wird. Dies ist nicht mit einer Antwort vergleichbar, da der Beitrag, den Sie starten, nicht unter dem ursprünglichen Beitrag angezeigt wird.

### Direktnachricht @KONTONAME

Dies ist wie die Erwähnungsfunktion, außer dass die Sichtbarkeit des gestarteten Beitrags auf "direkt" gesetzt wird.

---

### @KONTONAME stummschalten

Wenn Sie einen Nutzer stumm schalten, werden Toots von ihm und Toots, die ihn erwähnen, ausgeblendet, aber er kann Ihre Toots weiterhin sehen und Ihnen folgen.
Sie können auswählen, ob Sie weiterhin Benachrichtigungen erhalten, wenn der Nutzer mit Ihnen interagiert, und wie lange die Stummschaltung andauern soll.

### @KONTONAME blockieren

Durch das Blockieren eines Nutzers werden seine Toots und die Toots, die ihn erwähnen, ausgeblendet und der Nutzer kann Sie und Ihre Toots nicht mehr sehen und mit Ihnen interagieren.

### @KONTONAME melden

Das Melden eines Benutzers sendet einen Bericht an den Administrator Ihrer Instanz und - wenn Sie es wünschen - auch an den Administrator der Instanz, in der sich der Benutzer befindet.
Du kannst die Beiträge des Benutzers auswählen, die in den Bericht aufgenommen werden sollen, und ein wenig darüber schreiben, warum du ihn meldest.

### Alles von INSTANZNAME verstecken

Diese Funktion sperrt nicht nur den Benutzer, sondern die gesamte Instanz, in der er sich befindet. Dies wird z.B. verwendet, um Spam, Trolling und Belästigung durch eine ganze Instanz zu bekämpfen.
Wenn du eine Instanz sperrst, ist es normalerweise eine gute Idee, auch einen Bericht oder eine direkte Nachricht an deinen Instanzadministrator zu schicken, damit er entscheiden kann, ob er die Domain für alle in deiner Instanz sperren will.

---

### Löschen

Löscht den Beitrag, die Antworten die darauf gefolgt sind bleiben davon unberührt. Es kann nicht gewährleistet werden das der Beitrag im ganzen Fediverse gelöscht wird.

### Löschen und neu erstellen

Wie Löschen und kopiert den Text direkt in das Textfeld um den Beitrag neu zu erstellen, die Antworten die darauf gefolgt sind bleiben davon unberührt und werden damit ohne Kontext zurückgelassen.
D.h. es ist kein Bearbeiten im klassischen Sinne. Wenn schon Antworten unter dem Beitrag waren, werden diese nicht mit dem neuen Beitrag verknüpft und sind quasi verweist.
