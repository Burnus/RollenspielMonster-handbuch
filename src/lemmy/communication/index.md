# Kommunikation mit anderen Diensten

Hier wird aufgeschlüsselt, welche Funktionen von anderen Diensten bei Lemmy ankomme und wie diese interpretiert werden.

## Mastodon

Es ist möglich:

- auf bestehende Beiträge zu Antworten
- Beiträge zu favorisieren, dies wird in Lemmy als "Hochstimmen(Upvote)" eingetragen

Aktuell ist es nicht möglich:

- Beiträge in Communitys zu erstellen
- Communitys zu erstellen
- Bilder von Mastodon in Lemmy darzustellen
- Bilder von Lemmy in Mastodon darzustellen
- Texte zu formatieren

## Friendica

Es ist möglich:

- auf bestehende Beiträge zu Antworten
- Beiträge mit 👍 Mag ich oder 👎 Mag ich nicht zu bewerten, dies wird in Lemmy als "Hochstimmen(Upvote)" bzw. "Runterstimmen(Downvote)" eingetragen
- Bilder von Lemmy in Friendica darzustellen
- Beiträge in Communitys zu erstellen wenn der Beitrag mit einer Überschrift erstellt wird

Aktuell ist es nicht möglich:

- Communitys zu erstellen
- Bilder von Friendica in Lemmy darzustellen

## Hubzilla

Aktuell ist es nicht möglich:

- zwischen den Diensten zu kommunizieren

## Pixelfed

Aktuell ist es nicht möglich:

- zwischen den Diensten zu kommunizieren
- 
## PeerTube

Es ist möglich:

- PeerTube Kanälen zu folgen

Aktuell ist es nicht möglich:

- von PeerTube aus mit Lemmy zu kommunizieren