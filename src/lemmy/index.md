# Lemmy

Lemmy ist Seiten wie Reddit, Lobste.rs und Hacker News ähnlich: Du kannst Communitys folgen, die dich interessieren, Links posten und über diese diskutieren sowie sie bewerten.
Communitys funktionieren ähnlich wie Forenkategorien, also kann Lemmy auch gut als Forum verwendet werden.

Da es Teil des [Fediverse](/fediverse/) ist, können verschiedene Lemmy Instanzen miteinander kommunizieren.
