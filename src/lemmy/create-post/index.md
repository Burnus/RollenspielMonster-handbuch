# Beitrag erstellen

Um einen Beitrag zu veröffentlichen, klicke auf die Schaltfläche "Beitrag erstellen" am oberen Rand des Bildschirms. Sie werden zu einem Formular weitergeleitet, in dem Sie die URL eines Beitrags eingeben, ein Bild hochladen, einen Titel und einen Text für den Beitrag eingeben können. Sie müssen auch die Community angeben, in der Ihr Beitrag erscheinen soll. Bevor Sie eine Community auswählen, sollten Sie diese Communityseite aufrufen und die Richtlinien für Beiträge zu lesen, die auf der rechten Seite des Bildschirms erscheinen. So stellen Sie sicher, dass Sie sich an die Regeln der Community halten. In der Community !privacy@lemmy.ml ist es zum Beispiel nicht erlaubt, für proprietäre Software zu werben. Wenn Sie alle Felder ausgefüllt haben, klicken Sie auf "Erstellen" und Ihr Beitrag wird freigeschaltet.

**Bitte seht euch erst einmal nach passenden Communitys um in den ihr eure Beiträge veröffentlichen wollt oder erstellt entsprechende falls sie noch nicht vorhanden sind.** Dies erleichtert die Arbeit von Adiministratoren und Moderatoren erheblich.

Zum schreiben von Beiträgen oder Kommentaren sind folgende Tags verfügbar:

- @a_Benutzername, um eine Liste von Benutzernamen zu erhalten.
- !a_community, um eine Liste der Communitys zu erhalten.
- :emoji, um eine Liste von Emojis zu erhalten.

## Das Formular

### URL

**Optionales Feld.**
Link zu dem Thema über as ihr schreibt.

### Bild Symbol

Damit könnt ihr eurem Beitrag ein Banner verpassen.

### Titel

Um die Übersichtlichkeit zu erhören verwendet bitte Tags und prägnante Überschriften z.B. `[Frage] Wie erstelle ich einen Charakterbogen in Roll20`

#### Tags

Lemmy hat noch keine Tags, daher markiert man Beiträge am besten indem man kleine Textbaustein am Anfang des Titels setzt. z.B. mit [Frage], [Hilfe], [Projekt], [Sonstiges] oder etwas anderem, was Sie für angemessen halten.

### Text

Hier kommt eurer Text und/oder Frage rein. Lasst euch von dem kleinen Textfeld nicht abschrecken, dies vergrößert sich sobald ihr schreibt. Das Menü für die Textmanipulation taucht auch erst auf, nachdem ihr ein Zeichen in das Textfeld geschrieben habt.

### NSFW

Damit könnt ihr Beiträge ausblenden der andere verstören könnte. Z.B. gewalttätige oder sexuelle Szenen. Dies könnt ihr in den Benutzereinstellungen aktivieren oder deaktivieren.

### Community

Dies sind grob gesagt Kategorien. Bevor ihr einen Beitrag schreibt solltet ihr schauen ob es eine Community gibt, welche zu eurem Beitrag passt. Falls nicht könnt ihr jederzeit eine eigene eröffnen.
> Bitte kontrolliert vorher, ob es nicht bereits eine passende Community gibt.