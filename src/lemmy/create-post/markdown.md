# Markdown Spickzettel

---

- [Markdown Spickzettel](#markdown-spickzettel)
  - [Was ist Markdown?](#was-ist-markdown)
  - [Allgemeine Befehle](#allgemeine-befehle)
  - [Code](#code)
  - [Überschriften](#überschriften)
  - [Zeilenumbruch](#zeilenumbruch)
  - [Ungeordnete Listen](#ungeordnete-listen)
    - [Liste mit \*](#liste-mit-)
    - [Liste mit \* und Einrückungen](#liste-mit--und-einrückungen)
    - [Liste mit +](#liste-mit--1)
    - [Liste mit -](#liste-mit--)
  - [Geordnete Liste](#geordnete-liste)
  - [Zitate](#zitate)
  - [Verweise innerhalb des Dokuments](#verweise-innerhalb-des-dokuments)
    - [Verweis 1](#verweis-1)
    - [Verweis 2](#verweis-2)
    - [Verweis 3](#verweis-3)
  - [Externe Verweise](#externe-verweise)
  - [Verweise auf Abschnitte anderer Dokumente](#verweise-auf-abschnitte-anderer-dokumente)
  - [Bilder einfügen](#bilder-einfügen)
  - [Emojis](#emojis)
  - [Tabellen](#tabellen)
  - [Fußnoten](#fußnoten)

---

## Was ist Markdown?

Markdown ist für Menschen eine leicht verständliche und einfach zu erlernende Auszeichnungssprache.

## Allgemeine Befehle

Kursiv = `*Kursiv*`

Fett = `**Fett**`

Kursiver und fetter Text = `***Kursiver und fetter Text***`

~~Durchgestrichener Text~~ = `~~Dieser Text ist durchgestrichen.~~`

Horizontale Linie = `***`

Horizontale Linie = `---`

## Code

Code einfügen = `Am Anfang und am Ende einer Zeile`

Code Block - Möglichkeit 1:

```text
Code Block
```

` ```text `  
`Code Block`  
` ``` `

Code Block - Möglichkeit 2:

Einrücken um 4 Leerzeichen

    Test
    Test2

```markdown
    Test
    Test2
```

## Überschriften

Überschrift 1 = `# Überschrift 1`  
Überschrift 2 = `## Überschrift 2`  
Überschrift 3 = `### Überschrift 3`  
Überschrift 4 = `#### Überschrift 4`  
Überschrift 5 = `##### Überschrift 5`  
Überschrift 6 = `###### Überschrift 6`

## Zeilenumbruch

Ein Zeilenumbruch wird durch 2 Leerzeichen am Zeilenende erzwungen.

**Ohne Zeilenumbruch**  
Erste Zeile
Zweite Zeile

```Markdown
Erste Zeile
Zweite Zeile
```

**Mit Zeilenumbruch**  
Erste Zeile  
Zweite Zeile

```Markdown
Erste Zeile
Zweite Zeile
```

## Ungeordnete Listen

Um mit Markdown eine unsortierte Liste zu erstellen, könnt ihr die Zeichen Plus, Bindestrich oder ein Sternchen verwenden. [^1]

### Liste mit \*

- Listeneintrag
- Listeneintrag

```markdown
- Listeneintrag
- Listeneintrag
```

### Liste mit \* und Einrückungen

Die einzurückende Zeile ist mit **2** Leerzeichen einzurücken. Je nach verwendetem Markdown-Editor auch mit <kbd>Tabulator</kbd>

- Listeneintrag
- Listeneintrag
  - Listeneintrag
  - Listeneintrag

```markdown
- Listeneintrag
- Listeneintrag
  - Listeneintrag
  - Listeneintrag
```

### Liste mit +

- Listeneintrag
- Listeneintrag

```markdown
- Listeneintrag
- Listeneintrag
```

### Liste mit -

- Listeneintrag
- Listeneintrag

```markdown
- Listeneintrag
- Listeneintrag
```

## Geordnete Liste

```markdown
1. Line
2. Line
   1. Sub-Line
   2. Sub-Line
3. Line
4. Sub-Line
5. Sub-Line
```

## Zitate

> Zitatebene 1 - Zitat

> > Zitatebene 2 - Zitat im Zitat

> > > Zitatebene 3 - Zitat im Zitat im Zitat

```markdown
> Zitatebene 1 - Zitat

> > Zitatebene 2 - Zitat im Zitat

> > > Zitatebene 3 - Zitat im Zitat im Zitat
```

## Verweise innerhalb des Dokuments

### Verweis 1

### Verweis 2

### Verweis 3

1. [Verweis 1](#verweis-1)
2. [Verweis 2](#verweis-2)
3. [Verweis 3](#verweis-3)

Als Code sieht das Ganze dann so aus

```markdown
1. [Verweis 1](#verweis-1)
2. [Verweis 2](#verweis-2)
3. [Verweis 3](#verweis-3)
```

## Externe Verweise

Es lässt sich ein Link auf ein anderes Dokument einsetzen

[Wie benutzt man Lemmy](https://lemmy.rollenspiel.monster/post/3)

```markdown
[Wie benutzt man Lemmy](https://lemmy.rollenspiel.monster/post/3)
```

## Verweise auf Abschnitte anderer Dokumente

Verweise auf Abschnitte innerhalb anderer Dokumente sind ebenfalls möglich:

[Wie benutzt man Lemmy - Handbuch](https://lemmy.rollenspiel.monster/post/3#Handbuch)

```markdown
[Wie benutzt man Lemmy - Handbuch](https://lemmy.rollenspiel.monster/post/3#Handbuch)
```

## Bilder einfügen

Grundsätzlich könnt ihr bei Lemmy bilder per Drag and Drop oder Paste einfügen.

Der Syntax für das Einfügen bin Bildern sieht wie folgt aus.

```markdown
![](Bildurl)
```

## Emojis

Es gibt eine große Zahl an Emojis, die in Markdown-Dokumente eingefügt werden können.

⚠️ **ACHTUNG:**

```markdown
:warning: **ACHTUNG:**
```

😀 😄 🤣 😉

```markdown
:grinning: :smile: :rofl: :wink:
```

Auf GitHub finden sich einige gute Emoji-Cheat-Sheets

- [ikatyang / emoji-cheat-sheet ](https://github.com/ikatyang/emoji-cheat-sheet)
- [rxaviers](https://gist.github.com/rxaviers)/\*\*[gist:7360908](https://gist.github.com/rxaviers/7360908)

Für Visual Studio Code gibt es die Erweiterung `:emojisense`. Beim Einfügen eines `:` in das Dokument wird ein Dropdown eingeblendet und das entsprechende Emoji kann ausgewählt werden.

## Tabellen

Tabellen lassen sich sehr einfach schreiben.

| Spaltenüberschrift 1 | Spaltenüberschrift 2 | Spaltenüberschrift 3 |
| -------------------- | -------------------- | -------------------- |
| Inhalt               | Inhalt               | Inhalt               |
| Inhalt               | Inhalt               | Inhalt               |
| Inhalt               | Inhalt               | Inhalt               |

```markdown
| Spaltenüberschrift 1 | Spaltenüberschrift 2 | Spaltenüberschrift 3 |
| -------------------- | -------------------- | -------------------- |
| Inhalt               | Inhalt               | Inhalt               |
| Inhalt               | Inhalt               | Inhalt               |
| Inhalt               | Inhalt               | Inhalt               |
```

## Fußnoten

Ein Text mit einer Fußnote.[^1]  
[^1]: Der Text wird automatisch am Seitenende platziert.

```markdown
Ein Text mit einer Fußnote.[^1]
[^1]: Der Text wird automatisch am Seitenende platziert.
```

[Quelle](https://codeberg.org/strobelstefan.org/Markdown/src/branch/main/Markdown-Spickzettel.md)
