# Community erstellen Felder

## Name

**Kann nicht mehr geändet werden.**
Wird zur URL der Community. Nur Buchstaben, Zahlen und Unterstriche sind erlaubt. Maximal 16 Zeichen.

## Anzeigename

Wird als Titel der Community angezeigt.

## Icon

Kleines Bild das in der Communityliste vor dem Namen angezeigt wird

## Banner

Größeres Bild das in der Community übersicht oben angezeigt wird.

## Seitenleiste

Platz für eine Beschreibung oder Regelungen für die Community.

## NSFW

Damit könnt ihr Beiträge ausblenden der andere verstören könnte. Z.B. gewalttätige oder sexuelle Szenen. Dies könnt ihr in den Benutzereinstellungen aktivieren oder deaktivieren.