# Nextcloud

Nextcloud selbst bietet eine recht umfangreiche [Dokumentation](https://docs.nextcloud.com/server/latest/user_manual/de/) in Deutsch an, daher werde ich dies hier nur verlinken und nur auf die Besonderheiten unserer Instanz eingehen.

## Inaktive User

**Inaktive User werden gelöscht**, beachtet dazu die Angaben unter [User retention](/nextcloud/#account-retention)

## Teilen von Dateien

Die Standardablaufzeit für Freigaben ist 7 Tage, dies kann aber im Teilen dialog abgeändert werden.

## Apps

Hier werden die einzelnen erweiterungen aufgeschlüsselt welche installiert sind.

### Account retention

[App Store](https://apps.nextcloud.com/apps/user_retention)

**Benutzer** werden gelöscht, wenn sie sich innerhalb von **180 Tagen** nicht in ihr Konto eingeloggt haben. **Gäste** innerhalb von **60 Tagen**. Dabei werden auch **alle Dateien** der betroffenen Benutzer gelöscht.

### Calendar

[App Store](https://apps.nextcloud.com/apps/calendar)

Die Kalender-App ist eine Benutzeroberfläche für den CalDAV-Server von Nextcloud. Synchronisieren Sie ganz einfach Ereignisse von verschiedenen Geräten mit Ihrer Nextcloud und bearbeiten Sie sie online.

### Collectives

[App Store](https://apps.nextcloud.com/apps/collectives)

Collectives ist eine Nextcloud-App für Aktivisten und Gemeinschaftsprojekte, die sich gemeinsam organisieren. Kommen Sie und versammeln Sie sich in Kollektiven, um gemeinsames Wissen aufzubauen.

- 👥 Kollektive und nicht hierarchische Arbeitsabläufe von Herzen: Kollektive sind an einen Nextcloud Circle gebunden und gehören dem Kollektiv.
- 🔤 Bekannte Markdown-Syntax für die Seitenformatierung.

### Contacts

[App Store](https://apps.nextcloud.com/apps/contacts)

Die Nextcloud-Kontakte-App ist eine Benutzeroberfläche für den CardDAV-Server von Nextcloud. Synchronisieren Sie ganz einfach Kontakte von verschiedenen Geräten mit Ihrer Nextcloud und bearbeiten Sie sie online.

Contacts Interaction ist deaktiviert. (Sammeln von Daten über Benutzer- und Kontaktinteraktionen und Bereitstellung eines Adressbuchs für die Daten)

### Deck

[App Store](https://apps.nextcloud.com/apps/deck)

Deck ist ein Organisationstool im Kanban-Stil für die persönliche Planung und Projektorganisation von Teams, das in Nextcloud integriert ist.

### Forms

[App Store](https://apps.nextcloud.com/apps/forms)

Einfache Umfragen und Fragebögen, selbst gehostet!

- 📝 Einfaches Design: Keine Masse an Optionen, nur das Wesentliche. Funktioniert natürlich auch auf dem Handy.
- 📊 Ergebnisse anzeigen & exportieren: Die Ergebnisse werden visualisiert und können auch als CSV exportiert werden, im gleichen Format wie bei Google Forms.
- 🔒 Daten unter Ihrer Kontrolle! Anders als bei Google Forms, Typeform, Doodle und anderen bleiben die Umfragedaten und Antworten auf Ihrer Instanz privat.
- 🙋 Machen Sie mit! Wir haben viele Dinge geplant, wie weitere Fragetypen, Zusammenarbeit bei Formularen und vieles mehr!

### GPG Mailer

[App Store](https://apps.nextcloud.com/apps/gpgmailer)

Wenn der Benutzer den öffentlichen Schlüssel hochlädt, werden die E-Mails an diesen Benutzer verschlüsselt und signiert.

### Guests

[App Store](https://apps.nextcloud.com/apps/guests)

👥 Ermöglicht eine bessere Zusammenarbeit mit externen Nutzern, indem es Nutzern erlaubt, Gastkonten zu erstellen.

Gastkonten können über das Freigabemenü erstellt werden, indem man entweder die E-Mail-Adresse oder den Namen des Empfängers eingibt und "Gastkonto erstellen" wählt. Sobald die Freigabe erstellt ist, erhält der Gastbenutzer eine E-Mail-Benachrichtigung über die E-Mail mit einem Link, über den er sein Passwort festlegen kann.

Gastnutzer können nur auf Dateien zugreifen, die für sie freigegeben wurden, und keine Dateien außerhalb von Freigaben erstellen; außerdem stehen die für Gastkonten zugänglichen Anwendungen auf einer Whitelist.

### Mail

[App Store](https://apps.nextcloud.com/apps/mail)

💌 Eine Mail-App für Nextcloud

- 🚀 Integration mit anderen Nextcloud-Apps! Derzeit Kontakte, Kalender und Dateien - weitere werden folgen.
- 📥 Mehrere Mailkonten! Kein Problem mit einem schönen einheitlichen Posteingang. Verbinden Sie jedes IMAP-Konto.

### Maps

[App Store](https://apps.nextcloud.com/apps/maps)

Die ganze Welt passt in deine Wolke!

- 🗺 Schöne Karte: Mit OpenStreetMap und Leaflet können Sie zwischen Standardkarte, Satellitenkarte, topografischer Karte, dunklem Modus und sogar Aquarell wählen! 🎨
- ⭐ Favoriten: Speichern Sie Ihre Lieblingsorte, ganz privat! Eine Synchronisierung mit GNOME Maps und mobilen Anwendungen ist geplant.
- 🧭 Routenplanung: Entweder mit OSRM, GraphHopper oder Mapbox möglich.
- 🖼 Fotos auf der Karte: Keine langweiligen Diashows mehr, zeigen Sie einfach direkt, wo Sie waren!
- 🙋 Kontakte auf der Karte: Sehen Sie, wo Ihre Freunde wohnen und planen Sie Ihren nächsten Besuch.
- 📱 Geräte: Hast du dein Handy verloren? Schau auf der Karte nach!
- 〰 Tracks: Laden Sie GPS-Tracks oder vergangene Fahrten. Die Aufzeichnung mit PhoneTrack oder OwnTracks ist geplant.

### Metadata

[App Store](https://apps.nextcloud.com/apps/metadata)

Ein Plugin, das die Datei-Metadaten in der Dateidetail-Seitenleiste anzeigt.

### Nextcloud Office

[App Store](https://apps.nextcloud.com/apps/richdocuments)

Online Bearbeitung von Office Dokumenten

### Notes

[App Store](https://apps.nextcloud.com/apps/notes)

Die Notizen-App ist eine ablenkungsfreie Notizen-App für Nextcloud. Sie bietet Kategorien für eine bessere Organisation und unterstützt die Formatierung mit der Markdown-Syntax. Notizen werden als Dateien in Ihrer Nextcloud gespeichert, sodass Sie sie mit jedem Nextcloud-Client ansehen und bearbeiten können. Weitere Funktionen sind das Markieren von Notizen als Favoriten.

Zusätzlich steht bei uns die [QOwnNotesAPI](https://apps.nextcloud.com/apps/qownnotesapi) zur verfügung.

### Polls

[App Store](https://apps.nextcloud.com/apps/polls)

Eine App für Umfragen, ähnlich wie Doodle/Dudle, mit der Möglichkeit, den Zugang zu beschränken (Mitglieder, bestimmte Gruppen/Nutzer, versteckt und öffentlich).

### Quota warning

[App Store](https://apps.nextcloud.com/apps/quota_warning)

Diese App sendet Benachrichtigungen an die Nutzer, wenn sie 85, 90 und 95 % ihres Kontingents erreicht haben (wird einmal pro Tag überprüft).

### Registration

[App Store](https://apps.nextcloud.com/apps/registration)

Mit dieser App können Nutzer ein neues Konto registrieren.

### Talk

[App Store](https://apps.nextcloud.com/apps/spreed)

Chat, Video- und Audiokonferenzen mit WebRTC

### User migration

[App Store](https://apps.nextcloud.com/apps/user_migration)

Mit dieser App können Nutzer ganz einfach von einer Instanz zu einer anderen migrieren, indem sie einen Export ihres Kontos durchführen.

### Draw.io (deaktiviert)

[App Store](https://apps.nextcloud.com/apps/drawio)

Integriert Draw.io in Nextcloud

### Markdown Editor (deaktiviert)

[App Store](https://apps.nextcloud.com/apps/files_markdown)

Markdown Editor erweitert den Nextcloud-Texteditor um eine Live-Vorschau für Markdown-Dateien.
