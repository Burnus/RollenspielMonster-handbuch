# Einstellungen

Wenn Sie unten rechts auf die drei Punkte klicken, öffnet sich
folgendes Fenster:
![Einstellungen](img/settings.jpg)

Von Oben nach unten:

- Hier können Sie Ihren Namen und Ihre Mailadresse eingeben oder ändern.
- Qualitätseinstellungen der Videoübertragung: Im Falle einer schlechten Internetverbindung, können Sie hier die Videoauflösung einstellen. Für Teilnehmer\_innen ist eine niedrige Videoauflösung absolut ausreichend, außer diese wollen auch eine Folie o.a. zeigen.
- Vollbildmodus. Durch Drücken dieser Funktion wird der gesamte Monitor für „Jitsi Meet“ genutzt.
- YouTube Video teilen: Tragen Sie einen YouTube-Link ein und das Video wird geteilt. So können Sie gemeinsam ein Video anschauen.
- Hintergrundunschärfe - Wird nicht beschrieben, probieren Sie es aus...
- Einstellungen: Wie Punkt 1: nur, dass Sie hier bei den Geräteeinstellungen landen und nicht bei der Namenseinstellung
- Alle (Mikrofone) stummschalten: Hier können Sie mit einem Klick alle Mikrofone, bis auf Ihr eigenes, ausschalten. Machen Sie davon unbedingt Gebrauch, wenn Teilnehmer\_innen ihr Mikro nicht ausschalten und Sie Störgeräusche in der Leitung haben.
- Sprecherstatistik: Hier sehen Sie mit einem Klick nicht nur wer wie lange gesprochen hat, sondern auch sehr übersichtlich, wer teilnimmt, wenn im Vorfeld Namen in der Einstellung eingetragen worden sind.
