# Ein Konto erstellen

Um Identitäten, Veranstaltungen und Gruppen erstellen zu können, müssen Sie ein Konto erstellen.

## Anmelden

Wenn Sie eine Instanz gefunden haben, die Ihnen zusagt, klicken Sie auf die Schaltfläche **Anmelden** in der oberen Leiste. Dann:

1. Geben Sie Ihre E-Mail-Adresse ein.
* Geben Sie ein Passwort ein
* das Kästchen **Ich stimme den Instanzregeln und den Nutzungsbedingungen zu** ankreuzen (**nachdem** Sie sie gelesen habe, natürlich ;) )
* Klicken Sie auf die Schaltfläche **Registrieren**.

Sie erhalten dann eine E-Mail mit einem Link zur Aktivierung Ihres Kontos. Wenn Sie das nicht tun, klicken Sie auf den Link **Haben Sie die Anweisungen nicht erhalten?**.

![Anmeldeseite](img/create.png)

!!! note
    Sobald Ihr Konto aktiviert ist, müssen Sie auf die Schaltfläche **Anmelden** klicken.

## Freischaltung

Sobald die Aktivierung abgeschlossen ist, müssen Sie folgende Felder ausfüllen:

1. einen **Anzeigenamen** (erforderlich): was andere Leute sehen werden
* a **Benutzername** (erforderlich): Ihre eindeutige Kennung für Ihr Konto auf dieser und allen anderen Instanzen. Er ist so eindeutig wie eine E-Mail-Adresse, was es für andere Personen einfach macht, damit zu interagieren.
* Ein **Bio**: Erzählen Sie mehr über sich.

Wenn du alles ausgefüllt hast, klicke auf die Schaltfläche **Mein Profil erstellen**.

![Bioseiten-Einstellungen](img/created.png)

## Einstellungen

**Zeitzone**

Mobilizon verwendet Ihre Zeitzone, um sicherzustellen, dass Sie Benachrichtigungen für ein Ereignis zur richtigen Zeit erhalten. Wenn die ermittelte Zeitzone nicht korrekt ist, können Sie diese über die Schaltfläche **Meine Einstellungen verwalten** ändern.

Sie können diesen Parameter immer in [Ihren Kontoeinstellungen] (../Kontoeinstellungen/#Präferenzen) ändern.

**Teilnahmebenachrichtigungen**

Mobilizon schickt Ihnen eine E-Mail, wenn es bei den Veranstaltungen, an denen Sie teilnehmen, wichtige Änderungen gibt: Datum und Uhrzeit, Adresse, Bestätigung oder Absage, usw. Wenn Sie diese Benachrichtigungen nicht wünschen, deaktivieren Sie das Kontrollkästchen **Benachrichtigung am Tag der Veranstaltung**.

Wenn alles für Sie in Ordnung ist, klicken Sie auf die Schaltfläche **Alles klar, weiter geht's!**.

![Einstellungsbild](img/settings.png)

