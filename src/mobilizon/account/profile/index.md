# Ein Profil erstellen

Mobilizon bietet Ihnen die Möglichkeit, mit mehreren Identitäten zu arbeiten, indem Sie verschiedene Profile verwenden. Um ein neues Profil zu erstellen, gehen Sie folgendermaßen vor:

1. Gehen Sie zu Ihren Kontoeinstellungen (indem Sie auf Ihren Avatar und **Mein Konto** klicken)
* Klicken Sie auf **Neues Profil** in der linken Seitenleiste
* Klicken Sie auf die Schaltfläche **Zum Hochladen klicken**, um einen Avatar für Ihr neues Profil hinzuzufügen
* Geben Sie einen **Anzeigenamen** (erforderlich) ein: das, was andere Leute sehen werden
* Geben Sie einen **Benutzernamen** (erforderlich) ein: Ihre eindeutige Kennung für Ihr Konto auf dieser und allen anderen Instanzen. Er ist so eindeutig wie eine E-Mail-Adresse, was es für andere Personen einfach macht, damit zu interagieren.
* Gib einen **Bio** ein: erzähl den Leuten mehr über dich
* Klicke auf die Schaltfläche **Speichern**.

![rose boreal profile cration img](img/create.png)

Von nun an können Sie mit der einen oder anderen Identität an einer Veranstaltung teilnehmen oder eine erstellen.

!!! note
    Sie können zwischen Ihren verschiedenen Identitäten in der oberen Leiste wechseln, indem Sie auf Ihren Avatar klicken. Standardmäßig ist die Identität, die in der oberen Seitenleiste angezeigt wird, diejenige, die Sie für Ihre Aktionen ausgewählt haben.

!!! note
    Sie müssen die Seite aktualisieren, um Ihr neues Profilbild zu sehen.