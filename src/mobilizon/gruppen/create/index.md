# Eine Gruppe erstellen und bearbeiten

Gruppen sind Räume für die Koordination und Vorbereitung. Sie können verwendet werden, um die Organisation von Veranstaltungen zu verbessern und um Ihre Gemeinschaft zu verwalten.

## Erstellung

Um eine Gruppe zu erstellen, müssen Sie:

1. Klicken Sie auf die Schaltfläche **Meine Gruppen** in der oberen Menüleiste
* Klicken Sie auf die Schaltfläche **Gruppe erstellen**.
* (erforderlich) geben Sie Ihren Gruppennamen in das Feld **Gruppenanzeige** ein
* (erforderlich) geben Sie den gewünschten Verbundgruppennamen in das Feld **Verbundgruppenname** ein (oder behalten Sie einfach den Namen bei, der mit Ihrem Gruppennamen generiert wurde) (dies ist wie Ihr Verbundbenutzername für Gruppen. Damit kann Ihre Gruppe im Verbund gefunden werden und ist garantiert eindeutig)
* Geben Sie eine Beschreibung Ihrer Gruppe in das Feld **Beschreibung** ein.
* füge einen Avatar für deine Gruppe hinzu, indem du ein Bild auf deinem Gerät auswählst, indem du auf die Schaltfläche **Klick zum Hochladen** klickst
* füge ein Banner für deine Gruppe hinzu, indem du ein Bild auf deinem Gerät auswählst, indem du auf die Schaltfläche **Zum Hochladen klicken** klickst
* Klicken Sie auf die Schaltfläche **Meine Gruppe erstellen**.

![Erstellungsgruppe img](img/create.png)

## Einstellungen

### Öffentlich

Sie können auf Ihre Gruppeneinstellungen zugreifen, indem Sie:

1. Klicken Sie auf die Schaltfläche **Meine Gruppen** in der oberen Menüleiste
* Klicken Sie auf die entsprechende Gruppe in der Liste
* Klicken Sie auf die Schaltfläche **Gruppeneinstellungen** in Ihrem Gruppenbanner

In diesem Bereich können Sie Informationen bearbeiten, die Sie bei der Erstellung der Gruppe hinzugefügt haben (siehe oben), wie Gruppenname, Beschreibung, Avatar und Banner. Sie können auch:

* die **Gruppensichtbarkeit** ändern:
* **Überall im Web sichtbar**: Die Gruppe wird in den Suchergebnissen öffentlich aufgelistet und kann im Erkundungsbereich vorgeschlagen werden. Auf ihrer Seite werden nur öffentliche Informationen angezeigt.
**Nur über einen Link zugänglich**: Sie müssen die Gruppen-URL freigeben, damit andere Personen auf das Profil der Gruppe zugreifen können. Die Gruppe wird weder in der Mobilizonsuche noch in den normalen Suchmaschinen gefunden werden.
* Erlauben Sie den Beitritt zur Gruppe:
**Jeder kann beitreten**: Jeder, der Mitglied Ihrer Gruppe werden möchte, kann dies über Ihre Gruppenseite tun.
* **Manuell neue Mitglieder einladen**: Die einzige Möglichkeit für Ihre Gruppe, neue Mitglieder zu bekommen, ist, wenn ein Administrator sie einlädt
* **Gruppenadresse** (Standort) hinzufügen
* manuell neue Follower genehmigen (Follower erhalten neue öffentliche Ereignisse und Beiträge)
* Gruppenadresse hinzufügen
* die Gruppe löschen

![creation settings img](Img/settings1.png)
![creation settings img](Img/settings2.png)

### Mitglieder

In diesem Abschnitt können Sie [Rollen verwalten](../roles-group).

### Follower

In diesem Abschnitt können Sie [Gruppen-Follower verwalten](../group-manage-followers).