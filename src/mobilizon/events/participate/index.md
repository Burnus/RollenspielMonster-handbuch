# An einer Veranstaltung teilnehmen

## Mit einem Konto

Wenn Sie an einer Veranstaltung teilnehmen möchten:

1. Rufen Sie die Veranstaltungsseite auf (so ist es einfacher ;))
* Klicken Sie auf die Schaltfläche **Teilnehmen** unter dem Veranstaltungsbanner
* Wählen Sie das Profil aus, das Sie für diese Veranstaltung verwenden möchten

![Veranstaltungsteilnahme mit Konto img](img/account-participation.png)

Das war's! Die Schaltfläche **Teilnehmen** ist jetzt **Ich nehme teil**.

!!! note
    Wenn Sie auf die Schaltfläche **Teilnehmen** klicken, ohne eingeloggt zu sein, werden Sie aufgefordert, dies zu tun.

!!! info
    Wenn die Teilnahmegenehmigung aktiviert ist, müssen Sie warten, bis ein Organisator Ihre Teilnahme genehmigt hat.

## Anonym

!!! note
    Der Organisator muss die anonyme Teilnahme **erlauben**. Standardmäßig ist dies bei Veranstaltungen nicht möglich. ![anonyme Teilnahme](img/anonymous-activate.png) 

Um anonym an einer Veranstaltung teilzunehmen:

1. Rufen Sie die Veranstaltungsseite auf (so ist es einfacher ;))
* Klicken Sie auf die Schaltfläche **Teilnehmen** unterhalb des Veranstaltungsbanners
* Klicken Sie auf **Ich habe kein Mobilizon-Konto**, um mit Ihrer E-Mail-Adresse teilzunehmen
* gib deine E-Mail-Adresse in das Feld **Email** ein
* [Optional] schreiben Sie eine Nachricht an den/die Organisator(en); am besten immer einen (Nick)Namen, damit der/die Organisator(en) weiß um wen es geht.
* Klicken Sie auf **Email senden**

![anonyme Teilnahme img](img/anonymous-participation.png)

Sie erhalten dann eine E-Mail zur Bestätigung Ihrer E-Mail-Adresse. Klicken Sie auf **Bestätigen Sie meine E-Mail-Adresse**. Herzlichen Glückwunsch: Ihre Teilnahme wurde bestätigt!

### Erinnern Sie sich an meine Teilnahme in diesem Browser

**Optional**: Sie können das Kontrollkästchen **Meine Teilnahme in diesem Browser merken** aktivieren: Damit können Sie Ihren Teilnahmestatus auf der Veranstaltungsseite anzeigen und verwalten, wenn Sie dieses Gerät verwenden. **Bitte deaktivieren Sie dieses Kontrollkästchen**, wenn Sie ein öffentliches Gerät verwenden.

Wenn diese Option aktiviert ist, können Sie Ihre anonyme Teilnahme beenden, indem Sie auf die Schaltfläche **Anonyme Teilnahme beenden** unterhalb des Veranstaltungsbanners klicken.

!!! Info
    Wenn die Teilnahmegenehmigung aktiviert ist, müssen Sie warten, bis ein Organisator Ihre Teilnahme genehmigt.