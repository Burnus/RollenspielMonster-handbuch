# Teilnehmer der Veranstaltung verwalten

## Teilnehmer auflisten

Als Ersteller der Veranstaltung können Sie sehen, wer an einer Veranstaltung teilnimmt. Dazu können Sie entweder klicken:

* den Link **X Personen gehen** bzw **x/x freie Plätze** auf Ihrer Veranstaltungsseite:

![Anzahl der Teilnehmer - Veranstaltungsansicht](img/all-events.png)

* oder **Meine Veranstaltungen** in der oberen Menüleiste und den Link **Teilnahmen verwalten**:

![Ereignis-Aktionen-Bild](img/my-events.png)

![Ereignis-Teilnehmerliste](img/list.png)

Wie man hier sieht wird nur "Anonyme Teilnehmer:in" angegeben, daher ist der (Nick)Name als Kommentar so wichtig.

## Einen Teilnehmer ablehnen

Um Teilnehmer abzulehnen, gehen Sie auf die Teilnehmerliste und:

1. kreuzen Sie die Teilnehmer an, die Sie ablehnen möchten
* Klicken Sie auf die Schaltfläche **Teilnehmer ablehnen**.

Daraufhin wird das Label **Abgelehnt** angezeigt:

![image rejected particpant](img/rejected.png)

> !!! info
> 
> Der Teilnehmer erhält eine Benachrichtigung über diese Ablehnung per E-Mail.

## Teilnehmer genehmigen

Wenn die Option **Ich möchte jede Teilnahmeanfrage genehmigen** vom Veranstalter aktiviert ist, sehen die Teilnehmer ein Modal, in dem sie optional einen kurzen Text hinzufügen können und auf die Schaltfläche **Teilnahme bestätigen** klicken müssen:

Der Organisator kann die Teilnahme bestätigen, indem er [zur Teilnehmerliste](#list-participations) geht und
1. die Kontrollkästchen vor den Teilnehmern anklicken
* Anklicken von **Teilnehmer genehmigen**

Daraufhin wird das Label **Teilnehmer** angezeigt