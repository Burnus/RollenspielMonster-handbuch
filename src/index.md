# Handbuch für Anwender\_innen

Hier wird die Handhabung von den [RollenspielMonster](https://rollenspiel.monster/) Diensten erklärt.

**Diese Anleitung befindet sich im Aufbau.**

## ToDo

Alle ausgegrauten Navigationspunkte werden noch mit Inhalt gefüllt werden.

## Verbesserungen

Jeder kann helfen dieses Handbuch zu verbessern, hast du Fehler gefunden oder Verbesserungsvorschläge? Dann eröffne doch einfach ein [Issue](https://codeberg.org/RollenspielMonster/handbuch/issues). Oder trage direkt dazu bei und erstelle einen [Pull Request](https://codeberg.org/RollenspielMonster/handbuch/pulls) mit den gewünschten Änderungen.
Es gibt auch einen [Matrix Raum](https://matrix.to/#/#rm_handuch:tchncs.de) in dem man sich dann über Formulierungen und ähnliches streiten kann ;P

Wenn ihr nicht wisst wie man mit GIT umgeht, gibt es hier eine kleine Einführung: https://gnulinux.ch/erstellung-eines-pull-requests

## Mitwirkende

Danke an alle die sich beteiligt haben dieses Handbuch besser zu machen ![pino ❤️](img/pino-herz.png)

- [Beowulf](https://codeberg.org/Beowulf)

## Infos

Ursprüngliche Version von [gruene.social](https://github.com/netzbegruenung/handbuch.gruene.social).
