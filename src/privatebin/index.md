## PrivateBin

PrivateBin ist ein minimalistisches, quelloffenes Online-Pastebin, bei dem der Server keine Kenntnis von den eingefügten Daten hat. Die Daten werden im Browser mit 256bit AES im Galois Counter Modus ver- und entschlüsselt.
