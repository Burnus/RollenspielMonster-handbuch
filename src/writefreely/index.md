# Writefreely

WriteFreely ist eine makroblogging Plattform, die das [ActivityPub](https://de.wikipedia.org/wiki/ActivityPub) Protokoll unterstützt.
Es ist sehr simpel und intuitiv. Sie können es verwenden um Blogs, Tagebücher, Bücher oder auch Webseiten zu erstellen.
Sie können Ihre Seite für alle sichtbar machen, nur für Menschen, die den Link dazu haben oder sie mit einem Passwort schützen.

## Was gibt es für Einschränkungen?

### Föderation:

Sie können anderen Nutzer\_innen von Ihrem WriteFreely Account (noch) nicht folgen.
Menschen können Ihnen aber von anderen Teilen des Fediverse folgen und Ihre neuen Posts in deren Zeitleisten sehen.
Diese werden allerdings nur den Titel und einen Link zu dem Post sehen. Kommentare auf diesen Fediverse Post erscheinen nicht unter dem Artikel.

### Medien hochladen:

Du kannst Medien nicht direkt zu WriteFreely hochladen, also musst du Bilder und Videos von anderen Seiten einbinden. Du kannst z.B. Bilder auf Pixelfed und Videos auf PeerTube hochladen.

## Registrierung

Aufgrund des hohen Spam aufkommens bei dieser Software und der wenigen Schutzmöglichkeiten ist die Registrierung geschlossen. Wer gerne dort Bloggen möchte kann diesen [Einladungslink](https://blog.rollenspiel.monster/invite/vjcz51) benutzen. Falls dieser Abgelaufen ist, gerne bei mir [@Tealk](https://rollenspiel.social/@Tealk) einen Code zur Registrierung erfragen.