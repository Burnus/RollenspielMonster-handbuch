# Allgemeines

![Framework](https://img.shields.io/badge/Framework-mdBook-pink) ![Lizenz](https://img.shields.io/badge/Lizenz-CC0%201.0-orange)

Anleitung für die RollenspielMonster Dienste.

## Kommunikation

Um die Zusammenarbeit zu erleichtern gibt es einen Matrix Raum: https://matrix.to/#/#rm_handuch:tchncs.de

## Aufbau/Branches

- **master** ist der stand welcher über den pages Branch veröffentlicht wird, siehe dazu auch [pre-push git-branch mdbook](https://codeberg.org/Tealk/Git-Hooks_examples/src/branch/master/pre-push%20git-branch%20mdbook)
- **develope** ist der Entwicklungsbranch
- **pages** ist der Branch der die Webseite ausliefert